from typing import Union

from pyspark import SparkContext
from pyspark.sql import DataFrame, SQLContext, Window
from pyspark.sql import types as sql_types
from pyspark.sql.functions import col, sum, udf

S3_PREFIX = 'coding-challenge'


@udf(returnType=sql_types.IntegerType())  # type: ignore
def udf_result_calc(result: Union[int, None], lot_quantity: float) -> float:
    """
    Returns what the new lot amount should be after the sell transactions are processed
    """
    if result is None:
        # case: no sell transactions on the join
        return lot_quantity
    if result <= 0:
        # case: lot was fully sold
        return 0
    elif 0 < result <= lot_quantity:
        # case: lot was partially sold
        return result
    else:
        # case: the sell of the lots was exhausted before this row (date)
        return lot_quantity


def create_df(sql_context: SQLContext, file_name: str, schema: sql_types.StructType) -> DataFrame:
    df = sql_context.read.csv(f's3://{S3_PREFIX}/{file_name}',
                              schema=schema,
                              dateFormat='MM/dd/yyyy',
                              header=True)

    return df


def create_lots_df(sql_context: SQLContext) -> DataFrame:
    current_lots_schema = sql_types.StructType([
        sql_types.StructField("account_id", sql_types.IntegerType(), False),
        sql_types.StructField("security_id", sql_types.IntegerType(), False),
        sql_types.StructField("quantity", sql_types.IntegerType(), False),
        sql_types.StructField("lot_date", sql_types.DateType(), False),
        sql_types.StructField("price", sql_types.DecimalType(scale=2), False)])
    current_lots_df = create_df(sql_context, file_name='CurrentLots.csv', schema=current_lots_schema)
    return current_lots_df


def create_lots_window_df(lots_df: DataFrame) -> DataFrame:
    current_lots_window = Window.partitionBy('account_id', 'security_id').orderBy(
        col('lot_date').asc()).rowsBetween(
        Window.unboundedPreceding,
        Window.currentRow)
    current_lots_window = lots_df.select(col('account_id'),
                                         col('security_id'),
                                         col('lot_date'),
                                         col('quantity').alias('quantity_lot'),
                                         sum('quantity').over(current_lots_window).alias(
                                             "quantity_windowed"),
                                         col('price'))
    return current_lots_window


def process(sql_context):
    current_lots_df = create_lots_df(sql_context)

    current_lots_aggregated = create_lots_window_df(current_lots_df)

    transactions_schema = sql_types.StructType([
        sql_types.StructField("account_id", sql_types.IntegerType(), False),
        sql_types.StructField("security_id", sql_types.IntegerType(), False),
        sql_types.StructField("transaction_type", sql_types.StringType(), False),
        sql_types.StructField("transaction_date", sql_types.DateType(), False),
        sql_types.StructField("quantity", sql_types.IntegerType(), False),
        sql_types.StructField("price", sql_types.DecimalType(scale=2), False)])
    transactions_df = create_df(sql_context, file_name='Transactions.csv', schema=transactions_schema)

    transaction_date_base: str = transactions_df.first().transaction_date.strftime('%Y%m%d')

    transactions_sell_df = transactions_df.filter(transactions_df.transaction_type == 'SELL'
                                                  ).select(col('account_id').alias('trans_account_id'),
                                                           col('security_id').alias('trans_security_id'),
                                                           col('quantity'))

    joined_df = current_lots_aggregated.join(transactions_sell_df,
                                             (col('account_id') == col('trans_account_id')) &
                                             (col('security_id') == col('trans_security_id')),
                                             'left_outer')

    comparison_calculation = joined_df.withColumn('result_calculation',
                                                  (joined_df.quantity_windowed - joined_df.quantity))

    result_calc_evaluation = comparison_calculation.withColumn('new_calculated_lot',
                                                               udf_result_calc(col('result_calculation'),
                                                                               col('quantity_lot')))

    resulting_lots = result_calc_evaluation.filter(col('new_calculated_lot') > 0
                                                   ).select(col('account_id'),
                                                            col('security_id'),
                                                            col('new_calculated_lot').alias('quantity'),
                                                            col('lot_date'),
                                                            col('price')
                                                            )

    transactions_buy_df = transactions_df.filter(transactions_df.transaction_type == 'BUY')
    t = transactions_buy_df.select("account_id", "security_id",
                                   "quantity", col("transaction_date").alias("lot_date"),
                                   "price")
    resulting_lots_with_buys = resulting_lots.union(t)

    resulting_lots_with_buys_ordered = resulting_lots_with_buys.orderBy([col('account_id'),
                                                                         col('security_id'),
                                                                         col('lot_date')])

    resulting_lots_with_buys_ordered.coalesce(1).write.csv(f's3://{S3_PREFIX}/CurrentLots_{transaction_date_base}',
                                                           header='true',
                                                           mode='Overwrite',
                                                           dateFormat='MM/dd/yyyy')


def main():
    spark_context = SparkContext().getOrCreate()
    sql_context = SQLContext(spark_context)
    process(sql_context)


if __name__ == '__main__':
    main()
