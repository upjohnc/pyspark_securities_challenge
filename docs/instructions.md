CurrentLot.csv show the current balances of Securities for the AccountID, with the Date of purchase and quantity
Transactions.csv shows the new Securities transactions on the AccountID. It includes the transaction type, date , quantity and price

The process calculates daily balance of the Securities for the Account. We need to update the current lot with new transactions data based on FIFO basis, i.e. If the transaction is SELL(i.e. it should be deducted from the earliest transaction, and then from the second earliest)  and finally store the new balances as new CurrentLots_<Date> file.

The output should be stored in a single file, regardless of the size of the transactions.

Expected output is provided for reference.