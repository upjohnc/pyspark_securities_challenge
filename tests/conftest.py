import pytest
from pyspark import SparkContext
from pyspark.sql import SQLContext


@pytest.fixture(scope='session')
def spark_context():
    spark_context = SparkContext()
    yield spark_context
    spark_context.stop()


@pytest.fixture(scope='session')
def sql_context(spark_context):
    sql_context = SQLContext(spark_context)
    yield sql_context
