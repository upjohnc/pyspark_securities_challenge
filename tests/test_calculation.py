from datetime import date
from decimal import Decimal
from importlib import reload
from pathlib import Path
from typing import List

import pandas as pd
import pytest
from mock import patch
from pandas.testing import assert_frame_equal

import calculate_df

ROOT_DIR = Path(__file__).parent
DATA_DIR = (ROOT_DIR / 'data').resolve()


def assert_frame_equal_with_sort(results: pd.DataFrame, expected: pd.DataFrame, key_columns: List):
    results_sorted = results.sort_values(by=key_columns).reset_index(drop=True)
    expected_sorted = expected.sort_values(by=key_columns).reset_index(drop=True)
    assert_frame_equal(results_sorted,
                       expected_sorted,
                       check_dtype=False,
                       check_like=True)


def lots_df(sql_context, file_name, schema):
    file_str: str = str(DATA_DIR / f'{file_name}')
    df = sql_context.read.csv(file_str,
                              schema=schema,
                              dateFormat='MM/dd/yyyy',
                              header=True)

    return df


@pytest.mark.spark_context
@patch('calculate_df.create_df', side_effect=lots_df)
def test_lots_window_df(patch_df, sql_context):
    current_lots_df = calculate_df.create_lots_df(sql_context=sql_context)
    result = calculate_df.create_lots_window_df(current_lots_df)
    result_df = result.toPandas()
    result_df['price'] = result_df['price'].astype(float)

    # assert that the patched function called properly
    assert 'CurrentLots.csv' == patch_df.call_args.kwargs['file_name']
    patch_df.assert_called_once()

    expected_df = pd.read_csv(DATA_DIR / 'expected_window.csv', parse_dates=['lot_date'])
    expected_df['lot_date'] = expected_df['lot_date'].dt.date

    assert_frame_equal_with_sort(result_df, expected_df, ['account_id', 'security_id', 'lot_date'])


@pytest.fixture(scope='function')
def mock_udf():
    def dummy_udf(f):
        return f

    def mock_udf(f=None, returnType=None):
        return f if f else dummy_udf

    udf_patch = patch('pyspark.sql.functions.udf', mock_udf)
    udf_patch.start()
    reload(calculate_df)
    yield
    udf_patch.stop()
    reload(calculate_df)


def test_result_evaluation_none(mock_udf):
    """
    Test that original lot is returned if there is no matched from left outer join
    """
    result = None
    original_lot = 12.0
    assert calculate_df.udf_result_calc(result, original_lot) == original_lot


def test_result_evaluation_fully_sold(mock_udf):
    """
    Test that the original lot was fully sold
    """
    result = -10
    original_lot = 12.0
    assert calculate_df.udf_result_calc(result, original_lot) == 0


def test_result_evaluation_partially_sold(mock_udf):
    """
    Test that the original lot was partially sold
    """
    result = 5
    original_lot = 12.0
    assert calculate_df.udf_result_calc(result, original_lot) == result


def test_evaluation_no_sell(mock_udf):
    """
    Test that none of the original lot was sold (case: older lots covered sell transaction)
    """
    result = 20
    original_lot = 12.0
    assert calculate_df.udf_result_calc(result, original_lot) == original_lot
