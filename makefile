# note need make >= 3.8.2
SHELL := bash
.ONESHELL:
.RECIPEPREFIX = >

root_dir = ./src

test:
> PYTHONPATH=$(root_dir) pytest -v

test-spark-context:
> PYTHONPATH=$(root_dir) pytest -v -m spark_context

test-no-spark-context:
> PYTHONPATH=$(root_dir) pytest -v -m "not spark_context"

test-watch:
> find . -type f -name "*.py" | entr env PYTHONPATH=$(root_dir) pytest -v --disable-pytest-warnings

isort:
> isort -rc . .isort.cfg

mypy:
> mypy $(root_dir)
