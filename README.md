# Pyspark Securities Challenge


## Coding Challenge
The instructions are in `./docs/instructions.txt`.<br />
The challenge is to update a file of lots (securities holding) based on transactions for the day.
The rules are:  remove the lots that were sold (in transactions file), starting with the oldest dates.
Add new lots that were bought (in the transactions file).  The constraints of the challenge are only one transaction
per security per user in a day  - no more than on sell or buy and no buy and sell in the day for the user and security.

## Usage
- Place `CurrentLots.csv` and `Transactions.csv` (from `./src/data`) into the s3 bucket under the prefix `coding-challenge`
- Place `calculate_df.py` in your s3 bucket in a prefix such as `code`.  Then run the Glue job you set up.
- The result is the output files will be saved in the s3 bucket with the format of `CurrentLots_{date}`

## Tech Stack
- pyspark on python 3.7
- Spark 2.4
- AWS S3
- AWS Glue 1.0

## Solution
For the sell transactions, the solution that uses window partition on the CurrentLots and then does
a comparison to the sell transactions.
At the end the buy orders are simply appended to the resulting dataframe and saved to s3.

The window is partitioned by account and security and ordered by date asc.  The sum of the quantity is
the aggregated value.
That dataframe is joined (left outer) to the sell transactions dataframe.
A left outer joined was used to include the existing lots which are not in the sell transactions.
The sell quantity is subtracted from the windowed quantity.  The resulting cases from this calculation are:
- result is None : no sell occurred so the original lot is kept
- result is less than or equal to 0 : the sell was greater than the aggregated amount to that date and therefore that date has a 0 lot
- result is less than the original lot quantity : the lot has a new value because the sell amount did not sell all of this lot
- result is greater than original lot quantity : the sell quantity was covered by previous lots

## Assumption
- sell never exceeds accumulated lots - meaning don't have to check that the user is "oversold"
- an account does not have a sell and buy of a security in the same file
- all transactions in the file have the same date - no multiple days in one file
- only one sell transaction - don't need to sum up sell transactions before processing

